@extends('layouts.default')

	@section('content')
	 
	  <h1><a class="do_previous" href="{{ URL::to('dashboard') }}">&nbsp;<i class="fa fa-home">&nbsp;</i></a>&raquo; Cancel Account</h1>
	 
	  {{  Form::open(array('url' => 'account/cancel')) }}
	   <h1 style="text-align:center;">Please don't Go. We'll miss you around here!</h1>
	   <hr> 
	  <h4> Dear <?php if(Session::get('firstname') == NULL || Session::get('firstname') == ""){
						echo "My Account";
				}else{
						echo  Session::get('firstname');
					}?> , </br></br>
	  I'm Russell the Founder of ProperInvoice. We are sorry to know that you would like to cancel your account.
	  </br></br>ProperInvoice was started so that freelancers, contractors, small businesses and self employed people could easily create, send and track Invoices, Expenses, Quotes, Payments online. ProperInvoice was born out of a personal need and we are very passionate about it. We are constantly trying to make ProperInvoice simpler and better and we cannot do this without your help and support. 
	  </br></br>
	  We hope that you will stay and help us make ProperInvoice better.
	  </br></br>
	  If there is anything that you would like to let us know or anything that we can do to change your mind then please feel free to email me directly at <a href="mailto:russell@properinvoice.com" target="_top">russell@properinvoice.com</a>
	  </br></br>
	  We offer a forever free account that you will be automatically migrated to at the end of your subscription. Alternatively instead of cancelling your account you can downgrade your account using this <a href="{{ URL::to('subscription') }}">link</a>.
	  </h4>
	  </br>
	  <p>PS: Canceling your account will delete the account and all associated data permanently.This action is irrecoverable and we recomend you first export your data using this <a href="{{ URL::to('export') }}">link</a>.</p>
	  	
	  	<h4>Please enter your password to confirm. </h4>
	    <input type="password" name="password" value="" class="txt" />
	   <input type="hidden" name="cancel_token" value="{{ str_random(40); }}" />
	   
	   <input type="hidden" name="tenantID" value="{{ Auth::user()->get()->tenantID }}" />
	   <input type="hidden" name="super_user_id" value="{{ Auth::user()->get()->id }}" /><br />
	   <input type="submit" id="cancelaccount" class="gen_btn" name="cancelaccount" value="Cancel Account Permanently" />
	  <h4> </br>
	  Thanks,</br>
	  Russell</br>
	  Founder, ProperInvoice</br>
	  </h4>   
	{{ Form::close() }}  
			 
	@stop