<?php $tenant2 = Tenant::where('tenantID', '=', Session::get('tenantID'))->first();
?> <!-- {{ Company::where('tenantID','=', Session::get('tenantID'))->pluck('company_name') }} -->
<nav id="mobile">
    <div id="toggle-bar">
        <strong class="company-name mtoggle" style="text-align:center;"><i class="fa fa-bars" style="color:#0061CB !important;"></i> </strong> <img src="{{ URL::asset('proper_invoice_logo.png') }}" alt="." style="max-height:32px" ></a>
        <a class="mtoggle" href="#"></a>
    </div>

    <ul id="mmenu">
        <li><a href="{{ URL::to('dashboard') }}"><i class="fa fa-home"></i> Home</a></li>
        <li class=""><a href="#"><i class="fa fa-file-text"></i> Invoices</a>
            <ul class="sub-menu">
             <li><a href="{{ URL::to('invoices/create') }}"><i class="fa fa-plus-circle"></i> New Invoice</a></li>
             <li><a href="{{ URL::to('invoices') }}"><i class="fa fa-pencil-square-o"></i> Manage Invoices</a></li>
             </ul>
        </li>
        <li class=""><a href="#"><i class="fa fa-file-text-o"></i> Quotes</a>
            <ul class="sub-menu">
             <li><a href="{{ URL::to('quotes/create') }}"><i class="fa fa-plus-circle"></i> New Quote</a></li>
             <li><a href="{{ URL::to('quotes') }}"><i class="fa fa-pencil-square-o"></i> Manage Quotes</a></li>
             </ul>
        </li>
        
         <li class=""><a href="#"><i class="fa fa fa-money"></i> Expenses</a>
            <ul class="sub-menu">
             <li><a href="{{ URL::to('expenses/create') }}"><i class="fa fa-plus-circle"></i> New Expense</a></li>
             <li><a href="{{ URL::to('expenses') }}"><i class="fa fa-pencil-square-o"></i> Manage Expenses</a></li>
             </ul>
        </li>
        
        <li class=""><a href="#"><i class="fa fa fa-credit-card"></i> Payments</a>
            <ul class="sub-menu">
             <li><a href="{{ URL::to('invoices') }}"><i class="fa fa-pencil-square-o"></i> Manage Payments</a></li>
             <li><a href="{{ URL::to('paymentgateways') }}"><i class="fa fa-credit-card"></i> Payment Settings</a></li>
             </ul>
        </li>
        
     <!--  
        
        <li class=""><a href="#"><i class="fa fa-plus-circle"></i> Create</a>
            <ul class="sub-menu">
                <li><a href="{{ URL::to('invoices/create') }}"><i class="fa fa-file-text"></i> New Invoice</a></li>
                <li><a href="{{ URL::to('expenses/create') }}">New Expenses</a></li>
                <li><a href="{{ URL::to('quotes/create') }}">New Quote</a></li>
                <li><a href="{{ URL::to('clients/create') }}">New Client</a></li>
                <li><a href="{{ URL::to('services/create') }}">New Service</a></li>
                <li><a href="{{ URL::to('products/create') }}">New Product</a></li>
            </ul>
        </li>
        <li class=""><a href="#"><i class="fa fa-folder-open"></i> Manage</a>
            <ul class="sub-menu">
                <li><a href="{{ URL::to('invoices') }}">Invoices</a></li>
                <li><a href="{{ URL::to('expenses') }}">Expenses</a></li>
                <li><a href="{{ URL::to('quotes') }}">Quotes</a></li>
                <li><a href="{{ URL::to('clients') }}">Clients</a></li>
                <li><a href="{{ URL::to('services') }}">Services</a></li>
                <li><a href="{{ URL::to('products') }}">Products</a></li>
            </ul>
        </li>
        
        -->
        <li class=""><a href="#"><i class="fa fa-bar-chart-o"></i> Reports</a>
            <ul class="sub-menu">
                <li><a href="{{ URL::to('reports/summary') }}"><i class="fa fa-area-chart"></i> Financial summary</a></li>
                <?php if($tenant2->account_plan_id > 1): ?>
                <li><a href="{{ URL::to('reports/profit_and_loss') }}"><i class="fa fa-book"></i> Profit &amp; Loss</a></li>
                <?php endif; ?>
            </ul>
        </li>
        <li class=""><a href="#"><i class="fa fa-cog"></i> Settings</a>
            <ul class="sub-menu">
                <li><a href="{{ URL::to('company') }}"><i class="fa fa-building-o"></i> Business Profile</a></li>
                <li><a href="{{ URL::to('users/'.Session::get('user_id').'/edit') }}"><i class="fa fa-user"></i> My Profile</a></li>
                <li><a href="{{ URL::to('settings') }}"><i class="fa fa-cogs"></i> Account Settings</a></li>
               <!--
                <li><a href="{{ URL::to('currency-rates') }}">Currency Rates</a></li>
                <?php if($tenant2->account_plan_id > 1): ?>
                <li><a href="{{ URL::to('paymentgateways') }}">Payment Gateway</a></li>
                <?php endif; ?>
                
                -->
                <li><a href="{{ URL::to('settings/invoice_template') }}"><i class="fa fa-file-pdf-o"></i> Invoice Designs</a></li>
                <li><a href="{{ URL::to('subscription') }}"><i class="fa fa-bolt"></i> Subscription</a></li>
             <!--
                <li><a href="{{ URL::to('improvements') }}">Latest Updates</a></li>
                -->
            </ul>
        </li>
        <li><a href="{{ URL::route('logout') }}"><i class="fa fa-sign-out"></i> Logout</a></li>
    </ul>

</nav>