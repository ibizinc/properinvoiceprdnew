 
        <footer class="fluid-section">
           <div style="
    /* width: 100%; */
"> <div style="float:left;width: 190px;">
    <p class=""><a href="http://www.properinvoice.com" target="_blank"> ©2015 <span class="appversion">ProperInvoice</span></a> 
      </p></div><div class="footer-right" style="
    /* float: right; */
    background-color: #FFFFFF;
    /* width: 100%; */
    height: 32px;
    overflow: hidden;
    text-align: right;
    padding-right: 15px; line-height:32px; color:#dbd5d5;"><div style="float:left;padding-left: 10px;font-size: 25px;"><a href="http://www.facebook.com/ProperInvoice"target="_blank"><i class="fa fa-facebook-square" style="color:#637398;"></i></a> <a href="http://www.twitter.com/ProperInvoice"target="_blank"><i class="fa fa-twitter-square" style="color:#31beea;"></i></a> <a href="https://www.linkedin.com/company/properinvoice"target="_blank"><i class="fa fa-linkedin-square" style="color:#007fb4;"></i></a> <a href="https://plus.google.com/109636626424619894509/about"target="_blank"><i class="fa fa-google-plus-square" style="color:#cf3f2c;"></i></a></div> <div style="float:right;"> <a href="https://www.app.properinvoice.com/subscription" style="color:#0061CB; !important"> Subscription </a> | <a href="https://www.app.properinvoice.com/help" style="color:#0061CB; !important">Help </a> | <a href="https://www.app.properinvoice.com/support" style="color:#0061CB; !important">Feedback</a> | <a href="https://www.properinvoice.com/privacy" target="_blank" style="color:#0061CB; !important">Privacy</a> | <a href="https://status.properinvoice.com" target="_blank" style="color:#0061CB; !important">Status</a> | <a href="https://www.properinvoice.com/blog" target="_blank" style="color:#0061CB; !important">Blog</a> | <a href="https://www.app.properinvoice.com/support" style="color:#0061CB; !important">Support</a> | <a href="https://www.properinvoice.com/about-us" target="_blank" style="color:#0061CB; !important">About Us</a></div></div>
  </div>          
        </footer>
       
		</div><!-- END page-container -->
		
		<!-- Load JS here for greater good =============================--> 
	     
        <script src="{{ URL::asset('assets/js/jquery-1.8.3.min.js') }}"></script>  
        <script src="{{ URL::asset('assets/js/select2.js') }}"></script>     
        <script src="{{ URL::asset('assets/js/main.js') }}"></script>     
        <input type="hidden" class="app_theme"value="{{ Session::get('theme_id') }}">
        
        
        @yield('footer')
        <script src="{{ URL::asset('assets/js/jquery.popupoverlay.js') }}"></script>
        <script>
            $(window).load(function(){

                $('#mobile').css({'display':'block'});

                if($("#mmenu").length > 0){
                    $("#mmenu").hide();
                    $(".mtoggle").click(function() {
                        $("#mmenu").slideToggle(300);
                    });

                    $('#mmenu .sub-menu').hide(); //Hide children by default

                    $('#mmenu').children().click(function(){

                         $('#mmenu li').removeClass('navActive');
                         $('#mmenu li').addClass('navInActive');

                         $(this).addClass('navActive');
                         $(this).removeClass('navInActive');

                        //now find the `.child` elements that are direct children of the clicked `<li>` and toggle it into or out-of-view
                        $(this).children('.sub-menu').slideToggle(300);
                        $('.navInActive ul').slideUp('normal');
                    });

                    $(document).click(function(){
                        $('.sub-menu').slideUp('normal');
                    });

                    $("#mmenu").click(function(e) {
                        e.stopPropagation(); // This is the preferred method.
                        //return false;   // This should not be used unless you do not want
                        // any click events registering inside the div
                    });

                    var smallWindow = false;
                    $(window).on('resize', function () {
                        var windowsize = $(window).width();
                        if (windowsize > 800) {
                            $("#mmenu").slideUp('normal');
                        }
                    });

                }
        		
        		$('#go_invoice_form').click(function(){
        			 // Any Prep work
        		});
        	 
			 	$('input[name=business_model]').on('change', function() {
					
				   if($(this).val() == 1){
				   		
				   		$('#bill_option').fadeIn();
				   		
				   }else{
				   	
				   		$('#bill_option').fadeOut();
				   }
				 
				});

        	});
        </script>
    
    </body>
</html>