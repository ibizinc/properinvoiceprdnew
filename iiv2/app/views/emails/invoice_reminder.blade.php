@extends('layouts.sendinvoice')

	@section('email_title')
	 <h3> {{ $inv_email_subject }}</h3>
	@stop

	@section('content')
	 {{ $inv_email_body }}
	 
	 <br /><br />
	 <img width="170" height="50" style="color:#009999;display: block;margin-left: auto;margin-right: auto;width: 300px;" alt="Integrity Invoice"   src="https://www.app.properinvoice.com/proper_invoice_logo.png">
	 <small style="display: block;margin-right: auto;margin-left: auto;width: 13em;">Sent via <a href="http://www.properinvoice.com">properinvoice.com</a></small>
	@stop